# DarkWallet

SOVEREIGN. ANONYMOUS. UNCENSORED.

Anonymous financial system.

```console
$ git clone https://github.com/narodnik/darkwallet
$ cd darkwallet
$ cargo run --example simple
$ cargo test -- --nocapture
$ cargo build --example df
$ ./examples/anontx.sh
```

## Network Infra

Run these commands in separate terminals:

```console
$ cargo run --bin adamd
$ cargo run --bin titand
$ cargo run --example client
```

# Roadmap

1. Finalize API changes
2. Serialization and network services
3. Product

# Documentation

Run:

```console
$ cd doc/
$ make html
```

Requires sphinx Python documentation system.

# TODO

1. ~~`Proof::derive()` should be expired. Collect responses from witnesses. Use them to reinitialize `Proof` during proving stage.~~
1. ~~Split up `schema/asset.rs`~~
1. ~~Attributes as their own class with an index value~~
1. Make sure indexes passed to Coconut attributes are sane, otherwise fail. Shouldn't be asserts esp in server code.
1. Serializable types
1. ~~Tutorial documentation~~
1. Add smaller unit tests for `schema/` objects
1. API documentation
1. Daemonize processes, design protocol
1. Fix all the warnings
1. General code cleanup
1. Profile code sections

# Misc

Uses the [pairing library](https://electriccoin.co/blog/pairing-cryptography-in-rust/) written by ZCash project. Originally `pairing` library, now renamed to `bls12_381`.

Brought to you by **Dark Renaissance Technologies**.

