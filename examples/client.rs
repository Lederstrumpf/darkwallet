use aes_gcm::aead::{generic_array::GenericArray, Aead, NewAead};
use aes_gcm::Aes256Gcm;
use futures::io;
use futures::prelude::*;
use log::*;
use sha2::{Digest, Sha256};
use simplelog::*;
use smol::Async;
use std::collections::HashMap;

use std::marker::Unpin;
use std::net::{SocketAddr, TcpStream};
use std::sync::atomic::{AtomicU64, Ordering};

use darkwallet as dw;
use dw::Decodable;

struct Beacon {
    titand_address: SocketAddr,
}

type ConnectionsMap = async_dup::Arc<
    async_std::sync::Mutex<HashMap<SocketAddr, async_channel::Sender<dw::net::Message>>>,
>;

async fn fetch_beacon() -> dw::Result<Beacon> {
    let mut stream = Async::<TcpStream>::connect("127.0.0.1:7444").await?;
    let mut buffer = Vec::new();
    stream.read_to_end(&mut buffer).await?;

    let address = String::decode(&buffer[..])?;
    Ok(Beacon {
        titand_address: address.parse()?,
    })
}

async fn read_line<R: AsyncBufRead + Unpin>(reader: &mut R) -> io::Result<String> {
    let mut buf = String::new();
    let _ = reader.read_line(&mut buf).await?;
    Ok(buf.trim().to_string())
}

async fn menu() -> dw::Result<()> {
    // I am just hacking here
    // There are many things mixed altogether right now

    // Code below is the code we need for BLS DH algorithm
    // I just needed to write it somewhere, and didn't get
    // time yet to make a proper API or organize anything.

    let stdin = smol::reader(std::io::stdin());
    let mut stdin = io::BufReader::new(stdin);

    let g1 = dw::bls::G1Affine::generator();

    // Receiver
    // Create your secret and public keys

    //let secret = dw::bls::Scalar::new_random::<dw::OsRngInstance>();
    let recv_secret = dw::bls::Scalar::from_bytes(&[
        88, 55, 219, 28, 159, 44, 146, 250, 128, 87, 79, 93, 236, 219, 49, 225, 107, 185, 173, 167,
        214, 143, 43, 51, 144, 85, 206, 200, 85, 125, 8, 85,
    ])
    .unwrap();
    let recv_public = g1 * recv_secret;
    let bytes = recv_secret.to_bytes();
    info!("{:?}", bytes);

    // Sender
    // Create your secret and public keys

    //let ephem_secret = dw::bls::Scalar::new_random::<dw::OsRngInstance>();
    let ephem_secret = dw::bls::Scalar::from_bytes(&[
        216, 160, 83, 224, 82, 123, 113, 151, 189, 0, 64, 134, 213, 137, 75, 121, 172, 110, 209,
        153, 21, 58, 38, 154, 225, 153, 184, 194, 23, 98, 86, 93,
    ])
    .unwrap();
    let ephem_public = g1 * ephem_secret;
    let bytes = ephem_secret.to_bytes();
    info!("{:?}", bytes);

    // Sender creates derived secret key
    let derived_key = recv_public * ephem_secret;
    assert_eq!(derived_key, ephem_public * recv_secret);

    let mut hasher = Sha256::new();
    let data = dw::bls::G1Affine::from(derived_key).to_compressed();
    hasher.input(&data[0..32]);
    hasher.input(&data[32..]);
    let shared_secret = hasher.result();
    info!("shared_secret: {:?}", shared_secret.as_slice());

    // This is the value a receiver can use to see whether
    // an encrypted ciphertext belongs to them.
    let ss_hash = Sha256::digest(&shared_secret);
    info!("ss_hash: {:?}", ss_hash.as_slice());

    // Rust is gay, I need to convert to 'GenericArray' whatever the fuck that is...
    let key = GenericArray::from_slice(&shared_secret[..]);
    let cipher = Aes256Gcm::new(key);

    // 96-bits = 12 bytes; unique per message
    let nonce = GenericArray::from_slice(&ss_hash[..12]);

    // Create a simple ciphertext...
    let ciphertext = cipher
        .encrypt(nonce, b"plaintext message".as_ref())
        .expect("encryption failure!"); // NOTE: handle this error to avoid panics!

    // ... try to also decrypt it as a test
    let plaintext = cipher
        .decrypt(nonce, ciphertext.as_ref())
        .expect("decryption failure!"); // NOTE: handle this error to avoid panics!
                                        // OK it works!
    assert_eq!(&plaintext, b"plaintext message");

    // Send to titan:
    // emphem_public:48
    // ss_hash:4
    // ciphertext

    // This is our primitive block! (the slab data)

    // Find the TITAN address from ADAM
    let beacon = fetch_beacon().await?;
    info!("Address: {}", beacon.titand_address);

    // The primitive blockchain
    // Eventually this will be a PoS chain.
    // People will pay a token to put data inside.
    // For now it is a centralized single service
    let slabman = dw::net::SlabsManager::new();

    // Channel (queue) for sending data to the titan
    let (send_sx, send_rx) = async_channel::unbounded::<dw::net::Message>();

    send_sx
        .send(dw::net::Message::GetSlabs(dw::net::GetSlabsMessage {
            start_height: slabman.lock().await.last_height() + 1,
            end_height: 100000,
        }))
        .await?;

    // When we have PoS we will have multiple connections
    // We need this for broadcast, or selective sending
    // (for example broadcast to all connections except this one)
    let connections = async_dup::Arc::new(async_std::sync::Mutex::new(HashMap::new()));

    let send_sx2 = send_sx.clone();
    // Will refactor this later, for now still hacking
    let titan_task = smol::Task::spawn(async move {
        loop {
            match Async::<TcpStream>::connect(&beacon.titand_address).await {
                Ok(stream) => {
                    let stream = async_dup::Arc::new(stream);

                    let connections2 = connections.clone();
                    connections2
                        .lock()
                        .await
                        .insert(beacon.titand_address.clone(), send_sx2.clone());

                    // Run event loop
                    match process(
                        stream,
                        slabman.clone(),
                        connections2,
                        (send_sx2.clone(), send_rx.clone()),
                        &beacon.titand_address,
                    )
                    .await
                    {
                        Ok(()) => {
                            warn!("Server timeout");
                        }
                        Err(err) => {
                            warn!("Server disconnected: {}", err);
                        }
                    }
                    connections.lock().await.remove(&beacon.titand_address);
                }
                Err(err) => warn!("Unable to connect. Retrying: {}", err),
            }
            // Sleep 1 second, retry connect
            // Eventually we will have more complex connection strategies
            // This is temporary
            dw::net::sleep(1).await;
        }
    });

    // Menu screen
    'menu_select: loop {
        println!("[1] Send BTC");
        println!("[2] Quit");

        let buf = read_line(&mut stdin).await?;

        match &buf[..] {
            "1" => {
                // Send something
                let mut ss_hash_dst = [0u8; 4];
                ss_hash_dst.copy_from_slice(&ss_hash[0..4]);
                send_sx
                    .send(dw::net::Message::Put(dw::net::PutMessage {
                        ephem_public: dw::bls::G1Affine::from(ephem_public),
                        ss_hash: ss_hash_dst,
                        ciphertext: ciphertext.clone(),
                    }))
                    .await?;
                debug!("Send shite over");
            }
            "2" => break 'menu_select,
            _ => {}
        }
    }

    titan_task.cancel().await;

    info!("Shutting down...");
    //let _ = client.shutdown_sender.send(()).await;
    debug!("Shutdown signal sent.");

    Ok(())
}

type Clock = async_dup::Arc<AtomicU64>;

// Clients send repeated pings. Servers only respond with pong.
async fn repeat_ping(
    send_sx: async_channel::Sender<dw::net::Message>,
    clock: Clock,
) -> dw::Result<()> {
    loop {
        // Send ping
        send_sx.send(dw::net::Message::Ping).await?;
        debug!("send Message::Ping");
        clock.store(dw::get_current_time(), Ordering::Relaxed);

        dw::net::sleep(5).await;
    }
}

async fn process(
    mut stream: dw::net::AsyncTcpStream,
    slabman: dw::net::SlabsManagerSafe,
    _connections: ConnectionsMap,
    (send_sx, send_rx): (
        async_channel::Sender<dw::net::Message>,
        async_channel::Receiver<dw::net::Message>,
    ),
    _self_addr: &SocketAddr,
) -> dw::Result<()> {
    let inactivity_timer = dw::net::InactivityTimer::new();

    let clock = async_dup::Arc::new(AtomicU64::new(0));
    let ping_task = smol::Task::spawn(repeat_ping(send_sx.clone(), clock.clone()));

    loop {
        let event = dw::net::select_event(&mut stream, &send_rx, &inactivity_timer).await?;

        match event {
            dw::net::Event::Send(message) => {
                dw::net::send_message(&mut stream, message).await?;
            }
            dw::net::Event::Receive(message) => {
                inactivity_timer.reset().await?;
                protocol(message, &send_sx, &clock, &slabman).await?;
            }
            dw::net::Event::Timeout => break,
        }
    }

    ping_task.cancel().await;
    inactivity_timer.stop().await;

    // Connection timed out
    Ok(())
}

async fn protocol(
    message: dw::net::Message,
    send_sx: &async_channel::Sender<dw::net::Message>,
    clock: &Clock,
    slabman: &dw::net::SlabsManagerSafe,
) -> dw::Result<()> {
    match message {
        dw::net::Message::Ping => {
            // Ignore this message
        }
        dw::net::Message::Pong => {
            let current_time = dw::get_current_time();
            let elapsed = current_time - clock.load(Ordering::Relaxed);
            info!("Ping time: {} ms", elapsed);
        }
        dw::net::Message::Put(_message) => {
            //let message = dw::net::PutMessage::decode(Cursor::new(packet.payload))?;
            // Ignore this message
        }
        dw::net::Message::Inv(inv) => {
            // Store in index
            debug!("Received inv at height={}", inv.height);
            let mut slabman = slabman.lock().await;
            if slabman.has_unsorted_inv(inv.height) {
                debug!("Skipping already stored inv {}", inv.height);
                return Ok(());
            }
            // Code below can maybe be simplified/more elegant. Requires thinking though.
            if !slabman.has_cipher_hash(&inv.cipher_hash) {
                debug!("Fetching missing ciphertext {:x?}", inv.cipher_hash);
                send_sx
                    .send(dw::net::Message::GetCiphertext(
                        dw::net::GetCiphertextMessage {
                            cipher_hash: inv.cipher_hash.clone(),
                        },
                    ))
                    .await?;
                // No point organizing since we know the ciphertext is missing
                slabman.put_unsorted_inv(inv);
            } else if inv.height > slabman.last_height() {
                slabman.put_unsorted_inv(inv);
                slabman.organize();
            }

            if slabman.invs_are_missing() {
                debug!(
                    "Fetching missing invs from height {}",
                    slabman.last_height() + 1
                );
                send_sx
                    .send(dw::net::Message::GetSlabs(dw::net::GetSlabsMessage {
                        start_height: slabman.last_height() + 1,
                        end_height: slabman.min_missing_inv_height() - 1,
                    }))
                    .await?;
            }
        }
        dw::net::Message::GetSlabs(_message) => {
            // Ignore this message
        }
        dw::net::Message::GetCiphertext(_message) => {
            // Ignore this message
        }
        dw::net::Message::Ciphertext(ciphertext) => {
            // Add to local index
            let mut slabman = slabman.lock().await;
            slabman.put_ciphertext(ciphertext.ciphertext);
            slabman.organize();
            debug!(
                "Added missing ciphertext. Store now at {}",
                slabman.last_height()
            );
        }
    }

    Ok(())
}

fn main() -> dw::Result<()> {
    CombinedLogger::init(vec![
        TermLogger::new(LevelFilter::Debug, Config::default(), TerminalMode::Mixed).unwrap(),
        WriteLogger::new(
            LevelFilter::Debug,
            Config::default(),
            std::fs::File::create("/tmp/dfclient.log").unwrap(),
        ),
    ])
    .unwrap();

    info!("Started client.");

    smol::run(menu())
}
